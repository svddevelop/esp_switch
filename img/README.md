### Device

The PCB without elements

![PCB Top](IMG_20211202_091822(2).jpg){width=40 height=40}
![PCB Bottom](IMG_20211202_091854(2).jpg){width=40 height=40} 

![](connection.png)![](ESP12-2xSSR-3D-v2.png)




And full assembled with dificult custom extensions in the case

![device in case](IMG_20211202_130420.jpg){width=40 height=40}
![device in case](IMG_20211202_130358.jpg){width=40 height=40}
![extension for device](IMG_20211202_132305.jpg){width=40 height=40}
![device with extension in case](IMG_20211202_132246.jpg){width=40 height=40}


It can be mount in the wall-lamp

![The lamp, montage](IMG_20211202_092310.jpg){width=40 height=40}
![The lamp, in use](IMG_20211202_092334.jpg){width=40 height=40}



it can be use with manual switches of the old exist device, like torshere 

![the torshere](IMG_20211202_092415.jpg){width=40 height=40}
![the torshere,manual switches](IMG_20211202_092426.jpg){width=40 height=40}
![the torshere, the device](IMG_20211202_092442.jpg){width=40 height=40}