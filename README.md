## The ESP-Switch controller

What are on the board

* power from ~220V;
* up to two high-volts devices. The managment going over Solidstate Omron G3MC202P.
* the device can be expanded with switches, toggle buttons, diffrent sensors like summer, DHTxx, DS1820...
* the firmware have been ansynchronous code and could be integrated into any smarthome system over MQTT communication;
* the device can manage over http-protocol.

## Setup

At the first start (if SSID is not reachable or PSK is not correct) will be run access point with name "IOT<ChipID>", where "ChipID" is a unique identificator of ESP8266 and cannot be changed. The default password of access point is "12345678". The access point shows as default page the settings of WiFi.
![](https://bitbucket.org/svddevelop/esp01_ws2812b_led_strip/raw/89d3e05206ffad1786747b13b700f7f43079d154/img/Screenshot_20211218_151547.jpg)

After restart you can reach the WiFi-setting with the URL `http://<device_ip-or-name/wificfg` or update firmware with `http://<device_ip-or-name/upd`

## Setup the MQTT-settings

![](../img/)

## Setup the external setting

![](../img/)

## Start page over HTTP

![](../img/)

## Get the JSON description of the device over HTTP

## Change state of relay over HTTP


## Integration into real devices

### The wall-lamp 

![](img/IMG_20211202_092310.jpg)
![](img/IMG_20211202_092334.jpg)   
![](img/switch-http.jpg)

### Torshere

![](img/IMG_20211202_092415.jpg) 
![](img/IMG_20211202_092442.jpg)  
![](img/IMG_20211202_092426.jpg) 
![](img/torsher-domotics.jpg)
![](img/torsher-http.jpg)  
